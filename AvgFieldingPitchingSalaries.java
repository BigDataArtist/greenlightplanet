package JavaSpark.Javs.SQL;

import java.util.Properties;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class sparkSqlMysql {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(sparkSqlMysql.class);

	private static final SparkSession sparkSession = SparkSession.builder().master("local[*]").appName("Spark2JdbcDs")
			.getOrCreate();

	public static void main(String[] args) {
		// JDBC connection properties
		
		

		final Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "p@ssw0rd1@");
		connectionProperties.put("driver", "com.mysql.jdbc.Driver");
		final String dbTable = "(select * from Fielding) t";
		final String dbTable1 = "(select * from Salaries) m";
		final String dbTable2 = "(select * from Pitching) n";

		// Load MySQL query result as Dataset

		Dataset<Row> jdbcDF2 = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable,
				connectionProperties);
		Dataset<Row> jdbcDF3 = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable1,
				connectionProperties);

		Dataset<Row> jdbcDF4 = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable2,
				connectionProperties);

		jdbcDF2.createOrReplaceTempView("Fielding");
		jdbcDF3.createOrReplaceTempView("Salaries");
		jdbcDF4.createOrReplaceTempView("Pitching");

		Dataset<Row> sqlDF = sparkSession.sql(
				"select Salaries.yearID, round(avg(Salaries.salary)) as Fielding from Salaries inner join Fielding ON Salaries.yearID = Fielding.yearID  AND Salaries.playerID = Fielding.playerID group by Salaries.yearID limit 5");

		Dataset<Row> sqlDF1 = sparkSession.sql(
				"select Salaries.yearID, round(avg(Salaries.salary)) as Pitching from Salaries inner join Pitching ON Salaries.yearID = Pitching.yearID  AND Salaries.playerID = Pitching.playerID group by Salaries.yearID limit 5");


		sqlDF.createOrReplaceTempView("avg_fielding");

		sqlDF1.createOrReplaceTempView("avg_pitching");

		Dataset<Row> final_query_1_output = sparkSession.sql(
				"select avg_fielding.yearID, avg_fielding.Fielding, avg_pitching.Pitching from avg_fielding inner join  avg_pitching ON avg_pitching.yearID = avg_fielding.yearID limit 100");

		Dataset<Row> final_query_1_output2 = final_query_1_output;
		
		final_query_1_output2.write().csv("/Users/adityaverma/Desktop/avgFieldingSalariesPitching");;
		
	}

}
