package JavaSpark.Javs.SQL;

import java.util.Properties;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class AllStarHallOfFame {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(sparkSqlMysql.class);

	private static final SparkSession sparkSession = SparkSession.builder().master("local[*]").appName("Spark2JdbcDs")
			.getOrCreate();

	public static void main(String[] args) {
		// JDBC connection properties

		final Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "p@ssw0rd1@");
		connectionProperties.put("driver", "com.mysql.jdbc.Driver");
		final String dbTable = "(select * from Pitching) n";
		final String dbTable1 = "(select * from HallOfFame) h";
		final String dbTable2 = "(select * from AllstarFull) a";
		Dataset<Row> jdbcDF = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable,
				connectionProperties);
		Dataset<Row> jdbcDF1 = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable1,
				connectionProperties);
		Dataset<Row> jdbcDF2 = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable2,
				connectionProperties);

		jdbcDF1.createOrReplaceTempView("HallOfFame");
		jdbcDF2.createOrReplaceTempView("AllstarFull");
		jdbcDF.createOrReplaceTempView("Pitching");

		Dataset<Row> sqlDF1 = sparkSession.sql(
				"select HallOfFame.playerID,round(avg(Pitching.ERA)) as ERA_AVG,count(HallOfFame.playerID)as countPlayer from HallOfFame inner join AllstarFull on HallOfFame.playerID = AllstarFull.playerID inner join Pitching on HallOfFame.playerID = Pitching.playerID group by HallOfFame.playerID");
		// sqlDF1.show();

		Dataset<Row> sqlDF2 = sparkSession.sql("select playerID , yearID from HallOfFame where inducted ='Y'");

		// sqlDF2.show();

		sqlDF1.createOrReplaceTempView("results1");
		sqlDF2.createOrReplaceTempView("results2");

		Dataset<Row> final_query_1_output = sparkSession.sql(
				"select results1.playerId,results1.ERA_AVG,results1.countPlayer,results2.yearID from results1 inner join results2 on results1.playerID = results2.playerID limit 1000000");

		final_query_1_output.show();

		final_query_1_output.write().option("header", true).csv("/Users/adityaverma/Desktop/AllStarHallOfFame");
	}
}
