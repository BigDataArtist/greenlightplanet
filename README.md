                                            INSTALLATION
                                    
                              THIS IS A BATCH IMPLEMENTATION OF THE PROBLEM
                                    
                                             
                                               MYSQL:

 brew install mysql

 brew services start mysql

  Login as root and set privilages in Mysql.

                                          SPARK INSTALLATION:

  Minimum 4GB Ram Requirement.

  Follow this link to install spark in your local machine.
  
  https://www.tutorialkart.com/apache-spark/how-to-install-spark-on-mac-os/

  You can use spark submit to run the spark job from shell

                                               COMMAND:

  ./spark-2.1.0-bin-hadoop2.7/bin/spark-submit spakSql.jar

   The above command should be run from the terminal where we the same hadoop jar placed.

   Make sure the hadoop jar is placed or reffered from where you want to initiate your spark programs.


                                             EXECUTION STEPS:


  Here I have connected my SparkSql with Mysql database where I have the initial data.

  Connection is implemented through localhost JDBC connection.I used mysql-connector Jar to build the Jars

  The code is being run and written in the Eclipse IDE and can but also run from command Line.

  After Creating a connection with Mysql we have initiated the spark framework with using sparkSession.

  sparkSession gives us the levarge of using the spark libraries to run things more efficiently.
 
  SparkSql gives us the benifit of using the spark framework and still use native SQL Query advantage to run on top
  of it . So one only needs to implement the spark framework and create a session and start using their native SQL
  query for data processing.

  To use mysql Table as spark Table we have to create a temp view so that we can use SQl table to query in spark.

  Finally you can save the ouput in form of a csv file by using the following command

    ***final_query_1_output.write().option("header", true).csv("/Users/adityaverma/Desktop/yourDirectory");***


  To create a jar file of the Code you can export the jar file to the directory where you are running your
  Spark Submit Jobs.

  As it will run from the command line so you need to create a jar file.

                               
                                     REAL TIME IMPELEMENTATION OF THE PROBLEM



  In real time though I have given a Solution by Reading it through the MYSQL Database
  I would prefer reading it through some data source directly into my KAFKA TOPICS so that
  the data can be read through them for real time rather than having a batch oriented steps.
  
  You can find the implementation of the code in the file named below.
                              
                                           RealTimeIntegration.java

    
  Also as we have Different tables we can make Different Topis for each table and then these multiple 
  Topics can be used in spark to read the data into the diff spark dataframes which can be joined later
  to get the desired results.
    
  There is an Alternate Solution Also to it .We can completely used Ktables which is a concept of kafka Streams.
  Though each Dataframe has its pros and cons . One should really know what gto use when.
    
  I would prefer using Spark in this Scenario rather than Ktables . 
    
  As spark has a concept of Broadcasting Variables which can help us to avoid data shuffling everytime distribution
  over the executor nodes when a Query is veing triggered.
    
  It Keeps the copy of the data to each node hence avoids repeated network calls over the network.
    
    

