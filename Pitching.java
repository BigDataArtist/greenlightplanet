package JavaSpark.Javs.SQL;

import java.util.Properties;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Pitching {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(sparkSqlMysql.class);

	private static final SparkSession sparkSession = SparkSession.builder().master("local[*]").appName("Spark2JdbcDs")
			.getOrCreate();

	public static void main(String[] args) {
		// JDBC connection properties

		final Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "p@ssw0rd1@");
		connectionProperties.put("driver", "com.mysql.jdbc.Driver");
		final String dbTable = "(select * from Teams) t";

		Dataset<Row> jdbcDF = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", dbTable,
				connectionProperties);

		jdbcDF.createOrReplaceTempView("Teams");

		Dataset<Row> sqlDF = sparkSession.sql(
				"select HallOfFame.playerID, avg(Pitching.ERA),count(HallOfFame.playerID) from HallOfFame inner join AllstarFull on HallOfFame.yearID = AllstarFull.yearID and HallOfFame.playerID = AllstarFull.playerID inner join Pitching on HallOfFame.yearID = Pitching.yearID and HallOfFame.playerID = Pitching.playerID group by HallOfFame.playerID  ");

		sqlDF.show();

		

		Dataset<Row> sqlDF1 = sparkSession.sql("select teamID,YearID,AB,min(Rank) group by teamID,YearID,AB from Teams");
		sqlDF1.show();
		
		Dataset<Row> sqlDF2 = sparkSession.sql("Select yearID, min(rank) as Rank from Teams group by yearID Union Select yearID, max(rank) as Rank from Teams Group by yearID");
		sqlDF2.show();
		
		
		
		
	    Dataset<Row> sqlDf3 = sparkSession.sql("select Teams.teamID, A.yearID,A.Rank,Teams.AB from(Select yearID, min(rank) as Rank from Teams group by yearID Union Select yearID, max(rank) as Rank from Teams Group by yearID) A inner join Teams ON A.yearID = Teams.yearID and A.Rank = Teams.Rank");
		
	    sqlDf3.show();
	    
	    
	    
	    // Solutoion 2
	    
	    
	    
	    Dataset<Row> sqlDF4 = sparkSession.sql("\n" + 
	    		"SELECT Teams.teamID,Teams.yearID,Teams.Rank,Teams.AB FROM Teams "
	    		+ "JOIN (SELECT yearID\n" + 
	    		"             , MAX(Rank) AS max_rank\n" + 
	    		"          FROM Teams\n" + 
	    		"         GROUP BY yearID\n" + 
	    		"       ) AS derived\n" + 
	    		"    ON Teams.yearID = derived.yearID\n" + 
	    		"   AND Teams.Rank IN (1, derived.max_rank) limit 10" );
	    
	    
	    
	    sqlDF4.show();
	   
	    sqlDF4.write()..option("header", true).csv("/Users/adityaverma/Desktop/Pitching");
		
	    
	    
	    

	}
}
