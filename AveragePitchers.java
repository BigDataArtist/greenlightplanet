package JavaSpark.Javs.SQL;

import java.util.Properties;
import java.util.Vector;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class AveragePitchers {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(sparkSqlMysql.class);

	private static final SparkSession sparkSession = SparkSession.builder().master("local[*]").appName("Spark2JdbcDs")
			.getOrCreate();

	public static void main(String[] args) {
		// JDBC connection properties

		sparkSession.sparkContext().hadoopConfiguration().set("fs.s3n.awsAccessKeyId", "AKIAISJOGAH2ANF7PVOQ");
		sparkSession.sparkContext().hadoopConfiguration().set("fs.s3n.awsSecretAccessKey",
				"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");
		sparkSession.sparkContext().hadoopConfiguration().set("fs.s3.impl",
				"org.apache.hadoop.fs.s3native.NativeS3FileSystem");


		final Properties connectionProperties = new Properties();
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "p@ssw0rd1@");
		connectionProperties.put("driver", "com.mysql.jdbc.Driver");

		final String regSea = "(select * from Pitching) pre";
		final String postSea = "(select * from PitchingPost) post";

		// Load MySQL query result as Dataset

		Dataset<Row> RegularSeasonData = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", regSea,
				connectionProperties);

		Dataset<Row> PostSeasonData = sparkSession.read().jdbc("jdbc:mysql://localhost:3306/lahman2016", postSea,
				connectionProperties);

		RegularSeasonData.createOrReplaceTempView("Pitching");

		PostSeasonData.createOrReplaceTempView("PitchingPost");



		Dataset<Row> PostSeasonPitching = sparkSession
				.sql("select PitchingPost.yearID,PitchingPost.playerID ,round(avg(Pitching.ERA)) as regularseason , "
						+ "round(avg(Pitching.w/(Pitching.w+Pitching.l))) as regularWinLoss ,"
						+ "round(avg(PitchingPost.ERA)) as era_avg ,round(avg(PitchingPost.w/(PitchingPost.w+PitchingPost.l))) as era_winloss "
						+ "from PitchingPost inner join "
						+ " Pitching on PitchingPost.playerID = Pitching.playerID and "
						+ "PitchingPost.teamID = Pitching.teamID group by "
						+ "PitchingPost.yearID, PitchingPost.playerID order by PitchingPost.yearID asc limit 10");

		PostSeasonPitching.show();


		PostSeasonPitching.write().option("header", true).csv("/Users/adityaverma/Desktop/GL1");
		

	}
}

