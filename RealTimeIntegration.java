package JavaSpark.Javs.SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class RealTimeIntegration {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		String bootstrapServers = "127.0.0.1:9092";

		Properties properties = new Properties();

		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		// MYsql Connection

		String msg = null;
		String myDriver = "com.mysql.jdbc.Driver";
		String myUrl = "jdbc:mysql://localhost:3306/lahman2016";

		Class.forName(myDriver);

		Connection con = DriverManager.getConnection(myUrl, "root", "p@ssw0rd1@");

		String query = "Select * from Pitching";

		Statement st = con.createStatement();

		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			String PlayerID = rs.getString("playerID");
			double ERA = rs.getFloat("ERA");
			String mesg = PlayerID + " , " + ERA;
			System.out.println(mesg);

		
			KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

			ProducerRecord<String, String> record = new ProducerRecord<String, String>("DruidPush2", mesg);

			// This is Async process so we need use flush , otherwise the program finishes
			// and no data is being sent
			producer.send(record);

			producer.flush();
			producer.close();

		}
	
	}

}
